package com.paxotech.heatclinic;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by PaxoStudent on 4/29/2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        //dryRun = true,
        //strict = true,
        //monochrome = true,
        tags = { "@debug" },
        //tags = { "@functional","@debug" }, /*AND*/
        //tags = { "@functional,@debug" }, /*OR*/
        //tags = { "@functional,@debug","@debug" }, /*AND-OR*/
        features = "src/test/resources/features",
        glue = { "com.paxotech.heatclinic.framework.steps"},
        plugin={
                "pretty:target/cucumber-test-report/cucumber-pretty.txt",
                "html:target/cucumber-test-report",
                "json:target/cucumber-test-report/cucumber-report.json",
                "junit:target/cucumber-test-report/test-report.xml",
                "json:target/test-report.json"
        }
)
public class BDDHeatclinicRunner {
}
