package com.paxotech.heatclinic.framework.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by PaxoStudent on 4/29/2017.
 */
public class HeatclinicLoginSteps extends StepBase{





    @When("^Enter \"([^\"]*)\" as emaill address$")
    public void enter_as_emaill_address(String arg1) throws Throwable {
        driver.findElement(By.name("j_username")).sendKeys(arg1);
    }

    @When("^Enter \"([^\"]*)\" as password$")
    public void enter_as_password(String arg1) throws Throwable {
        driver.findElement(By.name("j_password")).sendKeys(arg1);
    }

    @When("^Click login$")
    public void click_login() throws Throwable {
       driver.findElement(By.cssSelector(".login_button.big.red")).click();
    }

    @Then("^Invalid email or password message display$")
    public void invalid_email_or_password_message_display() throws Throwable {
        WebElement error = driver.findElement(By.cssSelector(".error>p>span"));
        Assert.assertTrue(error.isDisplayed());
    }

    @Then("^Invalid email or password message not display$")
    public void invalidPasswordErrorNotDisplay(){

        WebElement error = null;
        try {
            error = driver.findElement(By.cssSelector(".error>p>span"));
        }
        catch (Exception ex){
        }

        if(error == null){
            Assert.assertTrue(true);
        }
        else {
            Assert.assertFalse(error.isDisplayed());
        }
    }
    @Then("^Welcome message shows \"([^\"]*)\"$")
    public void welcomeMggShowsWith(String msg){
        String actual = driver.findElement(By.cssSelector(".my-account")).getText();
        Assert.assertEquals(actual, msg);
    }
}
