package com.paxotech.heatclinic.framework.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by PaxoStudent on 4/30/2017.
 */
public class HomePageSteps extends StepBase{

    @Given("^As a not validated user$")
    public void notAValidUser() throws Throwable {
        driver.manage().deleteAllCookies();
    }

    @When("^Browse to the url \"([^\"]*)\"$")
    public void browse_to_the_url(String arg1) throws Throwable {
        driver.navigate().to(arg1);
    }
    @Then("^Heatclinic home page should show$")
    public void heatclinic_home_page_should_show() throws Throwable {

        String title = driver.findElement(By.xpath(".//*[@id='home_feature']/h2")).getText();
        Assert.assertEquals("HOT SAUCE AFICIONADO?",title);
    }

    @When("^User click login button$")
    public void user_click_login_button() throws Throwable {
        driver.findElement(By.linkText("Login")).click();
    }
}
