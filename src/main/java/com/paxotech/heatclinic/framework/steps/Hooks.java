package com.paxotech.heatclinic.framework.steps;

import com.paxotech.heatclinic.DriverFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by PaxoStudent on 4/30/2017.
 */
public class Hooks extends StepBase{
    @Before
    public void setUp(){
        //ChromeDriverManager.getInstance().setup();
        //driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void teardown(){
        //driver.close();
       // driver.quit();
        DriverFactory.getInstance().removeDriver();
    }
}
