package com.paxotech.heatclinic.framework.steps;

import com.paxotech.heatclinic.DriverFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by PaxoStudent on 4/30/2017.
 */
public class StepBase {

    protected  WebDriver driver = DriverFactory.getInstance().getDriver();

}
