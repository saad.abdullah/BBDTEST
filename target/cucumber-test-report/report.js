$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("HeatclinicLogin.feature");
formatter.feature({
  "line": 1,
  "name": "Heatclinic e-commerce Login Functionality",
  "description": "",
  "id": "heatclinic-e-commerce-login-functionality",
  "keyword": "Feature"
});
formatter.before({
  "duration": 6829824669,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "1. Invalid user valid password",
  "description": "",
  "id": "heatclinic-e-commerce-login-functionality;1.-invalid-user-valid-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@functional"
    },
    {
      "line": 4,
      "name": "@debug"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "As a not validated user",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Browse to the url \"http://heatclinic.shiftedtech.com/\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Heatclinic home page should show",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User click login button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Enter \"NotValid_paxqa2@Gmail.com\" as emaill address",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Enter \"paxotech\" as password",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Click login",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Invalid email or password message display",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.notAValidUser()"
});
formatter.result({
  "duration": 290027534,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://heatclinic.shiftedtech.com/",
      "offset": 19
    }
  ],
  "location": "HomePageSteps.browse_to_the_url(String)"
});
formatter.result({
  "duration": 1008904664,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.heatclinic_home_page_should_show()"
});
formatter.result({
  "duration": 103571671,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_click_login_button()"
});
formatter.result({
  "duration": 768805571,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NotValid_paxqa2@Gmail.com",
      "offset": 7
    }
  ],
  "location": "HeatclinicLoginSteps.enter_as_emaill_address(String)"
});
formatter.result({
  "duration": 417509827,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "paxotech",
      "offset": 7
    }
  ],
  "location": "HeatclinicLoginSteps.enter_as_password(String)"
});
formatter.result({
  "duration": 190453212,
  "status": "passed"
});
formatter.match({
  "location": "HeatclinicLoginSteps.click_login()"
});
formatter.result({
  "duration": 419874827,
  "status": "passed"
});
formatter.match({
  "location": "HeatclinicLoginSteps.invalid_email_or_password_message_display()"
});
formatter.result({
  "duration": 57925819,
  "status": "passed"
});
formatter.after({
  "duration": 251883496,
  "status": "passed"
});
formatter.before({
  "duration": 3911652708,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "3. Valid user valid password",
  "description": "",
  "id": "heatclinic-e-commerce-login-functionality;3.-valid-user-valid-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 26,
      "name": "@debug"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "As a not validated user",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "Browse to the url \"http://heatclinic.shiftedtech.com/\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "Heatclinic home page should show",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "User click login button",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "Enter \"iivaan@shiftedtech.com\" as emaill address",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "Enter \"shiftedtech\" as password",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "Click login",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Invalid email or password message not display",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "Welcome message shows \"IftekharX\"",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.notAValidUser()"
});
formatter.result({
  "duration": 97565198,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://heatclinic.shiftedtech.com/",
      "offset": 19
    }
  ],
  "location": "HomePageSteps.browse_to_the_url(String)"
});
formatter.result({
  "duration": 874245430,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.heatclinic_home_page_should_show()"
});
formatter.result({
  "duration": 92464591,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_click_login_button()"
});
formatter.result({
  "duration": 1422175670,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "iivaan@shiftedtech.com",
      "offset": 7
    }
  ],
  "location": "HeatclinicLoginSteps.enter_as_emaill_address(String)"
});
formatter.result({
  "duration": 315074711,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "shiftedtech",
      "offset": 7
    }
  ],
  "location": "HeatclinicLoginSteps.enter_as_password(String)"
});
formatter.result({
  "duration": 155794297,
  "status": "passed"
});
formatter.match({
  "location": "HeatclinicLoginSteps.click_login()"
});
formatter.result({
  "duration": 392633364,
  "status": "passed"
});
formatter.match({
  "location": "HeatclinicLoginSteps.invalidPasswordErrorNotDisplay()"
});
formatter.result({
  "duration": 1059730024,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IftekharX",
      "offset": 23
    }
  ],
  "location": "HeatclinicLoginSteps.welcomeMggShowsWith(String)"
});
formatter.result({
  "duration": 40717529,
  "error_message": "org.junit.ComparisonFailure: expected:\u003cIftekhar[]\u003e but was:\u003cIftekhar[X]\u003e\n\tat org.junit.Assert.assertEquals(Assert.java:115)\n\tat org.junit.Assert.assertEquals(Assert.java:144)\n\tat com.paxotech.heatclinic.framework.steps.HeatclinicLoginSteps.welcomeMggShowsWith(HeatclinicLoginSteps.java:68)\n\tat ✽.And Welcome message shows \"IftekharX\"(HeatclinicLogin.feature:36)\n",
  "status": "failed"
});
formatter.after({
  "duration": 259755695,
  "status": "passed"
});
});